package com.myorg;

import software.amazon.awscdk.core.App;

public final class SampleAppApp {
    public static void main(final String[] args) {
        App app = new App();

        new SampleAppStack(app, "SampleAppStack");

        app.synth();
    }
}
